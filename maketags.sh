#!/bin/sh
set -e #not complete but will work..

WHO=`whoami`
PROJS=(`ls -d /home/${WHO}/Documents/Developer/*/`)

var=0
for proj in ${PROJS[@]}
do
  var=$((var + 1))
  echo "$var. `basename ${proj}`" 
done


projNum=0
size=${#PROJS[@]}
while  [[ ! ($projNum -le ${#PROJS[@]} &&  $projNum -gt 0) ]]   
do
echo -n "Choose project then press [ENTER]: "
read projNum
done

PROJECT=${PROJS[$projNum-1]} 
if [ ! -d ${PROJECT}c_tag_info ]
then  
  mkdir ${PROJECT}c_tag_info
fi

find $PROJECT -name '*.h' -o -name '*.cpp' -o -name '*.cc' > ${PROJECT}c_tag_info/filelist.txt

ctags --sort=yes -L ${PROJECT}c_tag_info/filelist.txt -f ${PROJECT}c_tag_info/tags