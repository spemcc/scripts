#!/usr/bin/perl -w
#	
# Author: smccrear
#

=head1 SYNOPSIS

headercreate I<filename>

headercreate -a  I<filename>

=cut

use Getopt::Long;
use Pod::Usage;
use Cwd;

my $all='';
GetOptions ("all|a" => \$all);

if($#ARGV == -1){
	pod2usage(1);
}

my $curdir = getcwd;
my $cur_time = localtime;
my $filename =	shift ;
my $filenamewithpath = $curdir . "/". $filename. ".h";

if(-e $filenamewithpath){ 
print "File Exists.. Aborting!\n";
exit;
}

print "Creating file...\n";
open  FILE,"+>",$filenamewithpath or die $!;
print FILE "/* " . "Created on: $cur_time */\n";
print FILE "/* " . "Author: smccrear */\n\n";
print FILE "#ifndef " . "_" . uc($filename) . "_\n";
print FILE "#define " . "_" . uc($filename) . "_\n\n\n";
print FILE "class $filename {\n";
print FILE "	public:\n";
print FILE "		$filename()\;\n";
print FILE "		~" . "$filename()\;\n";
print FILE "  private:\n\n";
print FILE "  protected:\n";
print FILE "};\n\n\n";
print FILE "#endif  /* _" .uc($filename). "_ */\n\n";
close FILE               
      or warn $! ? "Error closing sort pipe: $!" : "Exit status $? from sort";

if($all)
{
	my $filenamewithpathcc = $curdir . "/". $filename. ".cc";
	if(-e $filenamewithpathcc)
	{ 
		print "File Exists.. Aborting!\n";
		exit;
	}
	open  FILE,"+>",$filenamewithpathcc or die $!;
	print FILE "\n\n#include \"" . $filename . ".h \"\n\n";
	print FILE $filename . "::" . $filename . "()\n{\n\n}\n\n" ;
	print FILE $filename . "::~" . $filename . "() \n{\n\n}";
	close FILE or warn $! ? "Error closing sort pipe: $!" : "Exit status $? from sort";
}









